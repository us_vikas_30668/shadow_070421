package com.example.shadowtask070421.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.example.shadowtask070421.R;
import com.example.shadowtask070421.databinding.ActivityMainBinding;
import com.example.shadowtask070421.databinding.ActivityMainBindingImpl;
import com.example.shadowtask070421.model.Device;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Device device1 = new Device("Laptop", "Lenovo", "L14 Thinkpad", 499, R.drawable.shopping);
        activityMainBinding.setDevice(device1);
        activityMainBinding.setDeviceName("" + device1.getManufacturer() + " " + device1.getModel());
    }
}
