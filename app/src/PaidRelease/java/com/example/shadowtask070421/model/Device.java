package com.example.shadowtask070421.model;

public class Device {

    private String category;
    private String manufacturer;
    private String model;
    private int priceInDollars;
    private int imageId;

    public Device(String category, String manufacturer, String model, int priceInDollars, int imageId) {
        this.category = category;
        this.manufacturer = manufacturer;
        this.model = model;
        this.priceInDollars = priceInDollars;
        this.imageId = imageId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPriceInDollars() {
        return priceInDollars;
    }

    public void setPriceInDollars(int priceInDollars) {
        this.priceInDollars = priceInDollars;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
