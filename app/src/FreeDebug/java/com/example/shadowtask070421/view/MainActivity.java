package com.example.shadowtask070421.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shadowtask070421.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
